require "rails_helper"

describe "games/new.html.erb" do
  before do
    @game = Game.new(player1: 'Bob', player2: 'Alice')
  end


  it "displays the blank board after hitting the new game" do

    #assign(:game, Game.create(player1: 'Shirt', player2: 'newplayer'))
    visit "/games/new"
    render
    #print rendered
    fill_in 'Player1', with: 'Bob'
    fill_in 'Player2', with: 'Alice'
    click_button 'Create Game'
    page.should have_content('Game was successfully created')
    page.should have_content('Player1: Bob Player2: Alice')
  end
end 