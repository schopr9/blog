require "rails_helper"

describe "games/show.html.erb" do

  before do
    @game = Game.new(player1: 'player1',player2: 'player2')
    @game.board =  [ "X","O","X","O"," ","O","O","O","O" ] 
    @game.save
  end

  it "displays the board" do
    game = double("game")
    expect(Game).to receive(:find).and_return(@game)
    visit "games/4"
    render    
    expect(rendered).to include("player1")
    expect(rendered).to include("player2")
    expect(rendered).to include("<li id='i0' class='btn span1'>")
    expect(find_by_id("i0").text).to eq('X Edit')
    expect(find_by_id("i1").text).to eq('O Edit')
    expect(find_by_id("i2").text).to eq('X Edit')
    expect(find_by_id("i3").text).to eq('O Edit')
    expect(find_by_id("i4").text).to eq('Edit')
    expect(find_by_id("i5").text).to eq('O Edit')

  end
end 

describe "games/show.html.erb" do

  before do
    @game = Game.new(player1: 'player1',player2: 'player2')
    @game.board =  [ "","","",""," ","","","","" ] 
    @game.save
  end

  it "update the board should show X" do
    game = double("game")
    expect(Game).to receive(:find).exactly(3).times.and_return(@game)
    visit "games/1"
    render    
    expect(rendered).to include("player1")
    expect(rendered).to include("player2")
    expect(rendered).to include("<li id='i0' class='btn span1'>")
    find_by_id("i5").click
    visit "1/edit"
    expect(find_by_id("i0").text).to eq('x Edit')
  end
end 