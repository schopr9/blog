class ChangeDataTypeForBoard < ActiveRecord::Migration
  def change
    change_table :games do |t|
      t.change :board, :string, array: true, default: []
    end  
  end
end
