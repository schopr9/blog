require_relative  './games_tt.rb'
class GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :update, :destroy]
  
  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end

  # GET /games/new
  def new
    @game = Game.new
  end

  # GET /games/1/edit
  def edit
    @boardId = params["boardId"]
    logger.debug "board : #{params["master"]} "
    @master = GameMaster.new(params[:player1],params[:player2])
    @master.game.go(@boardId.to_i + 1)
    @game.board = @master.board.board
    logger.debug "Person attributes hash: #{params["boardId"]} #{@master.board.board}"
    respond_to do |format|
      if @game.save
        format.html { redirect_to game_url(@game.id), notice: 'board is updated for position.' + @boardId.to_s }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { redirect_to game_url(@game.id), notice: 'Game was successfully created. #{@boardId}' }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
    
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)
    @game.board = [ "X","O","X","O"," ","O","O","O","O" ]

    respond_to do |format|
      if @game.save
        format.html { redirect_to game_url(@game.id), notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    @dummyBoard = [ "","","",""," ","","","","" ]
    respond_to do |format|      
      if @game.update_attributes(:board => @dummyBoard)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:player1, :player2)
    end
end
