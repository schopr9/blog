# README #

This README would normally document whatever steps are necessary to get your application up and running.

* Quick summary
This is a blog where you can write your post and only you can see it, just as a personal diary online

i want to use it as learning of the day

* Version
ruby on rails 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###

* Summary of set up
bundle install

rake db:migrate

rails server
* Configuration

no change required

    Title: Course Practice Overview for CS4472 Fall 2016
    Author: Saurabh Chopra
    Email: schopr9@uwo.ca  


Section 1: Introductory Remarks
Being a master’s student, this course was pretty challenging as it is purely practical oriented course. I have learnt a lot about testing, specifications and quality assurance. There is lot of material on the internet therefore we used Atlassian confluence to organize the material so that relevant information can be found easily. The good part about the course is that we have a choice for the task that we want to work on and if we change any issue with some task then you can ask for help on confluence. A variety of subjects were covered in the course like Ruby, JavaScript, Rails and various testing approaches.


Section 2: Current View of Role of Specifications, Testing, and Quality Assurance in My Software Development Process (at most 600 words prioritized to reflect what you think is most significant to say and concrete so that it is clear what is being said)


Although billions of dollars are spent trying to develop quality software, software bugs are very common. For most of the computer systems, the cost of software constitutes a major portion of the cost of the system. Since software is so important and valuable, if software development process lacks quality, then the software that’s developed will surely lack quality. That’s what I feel, however the term testing lies in every field and every industry. It is an essential factor in development cycle and a never-ending process. 
A lot of effort these days is put into automated testing like what we have used in the course cucumber and gherkin. Even after automated testing, we need to perform user acceptance testing which is done manually. In my view, the bug cost can be reduced if we can find the bugs at the early stages of development life cycle otherwise the bug gets costlier in the later stages as you will have to follow all the procedure of software development life cycle to fix that bug.
According to Pareto principle 80/20 rule which states that 80 % of defects are due to 20% of the causes. Therefore if we can identify these 20% of the causes then we can control 80% of the defects.
The review process helps to curb defects at each level of development , therefore it should not be skipped at any level of software development process. 
Code coverage is also a very important factor in testing and we learn in mutation testing how 100% covered code can still leak errors.


Section 3a: How View Has Changed This Semester Based on Course Practice (at most 300 words prioritized to reflect what you think is most significant to say and concrete so that it is clear what is being said)
Being a software developer for more than 6 years, it was mandatory for me to learn testing in detail. I have learnt a lot about programming languages, testing frameworks and most important implementing that in real world applications. Task which I did build my confidence, importantly when I got trapped in errors and then try to solve those errors to make it work. I believe that theoretical knowledge is not enough, instead a blend of theoretical and practical is more important. 
I learned Ruby and how to create a web application in ruby on rails, then I pushed a website to herokuapp.com i.e. https://ti-ctac-toe.herokuapp.com/ which is blog plus tic-tac-toe game using devise authentication and postgres db on herokuapp. A lot of effort was put into how to test your application using Rspec.
Section 3b: How View Has Changed This Semester Based on Class Readings, Videos, and Discussions (at most 300 words prioritized to reflect what you think is most significant to say and concrete so that it is clear what is being said
As I mentioned above, Weekly course practice is somewhat challenging. Watching videos and learning through that was quite interesting. In addition to that, whenever I faced any problems I searched on internet which made me understand what went wrong based on people's comments on the same problem. Thus, class reading and videos or even discussion with colleagues helped me in a positive way.




Section 3c: How View Has Changed This Semester Based on Activities Outside of Class (at most 300 words prioritized to reflect what you think is most significant to say and concrete so that it is clear what is being said)
This course taught me in detail approach of testing, specifications and quality assurance. Based on my gained knowledge, I started implementing testing approach in all my technical courses, specially software courses. It was really helpful.
I suggested in my company, where I’m working as an intern, to use cucumber in JavaScript for automated testing. Currently they are using mocha with sinon and chai for unit testing and there is no automation for linked testing. All the API and UI are tested manually by the testers but to reduce the overhead of testing, which has already been tested, is to automate system integration testing for regression purpose.


***** Section 4: Concluding Remarks *****
As complete efficient software would be one which has undergone through all possible testing approaches. According to me, without testing, one cannot get quality product, thus one should learn testing, specifications and quality assurance and apply the same. 
JIRA and confluence is used by all the software companies so it was a good exposure to use Atlassian products. 




Section 5: References (for any material referred to that is not on the course wiki pages)
JavaScript design patterns - https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know
JavaScript race condition - http://stackoverflow.com/questions/338110/avoiding-a-javascript-race-condition
Rails scaffolding - https://www.tutorialspoint.com/ruby-on-rails/rails-scaffolding.htm